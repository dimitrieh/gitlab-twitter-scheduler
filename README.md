# Tweeting from Gitlab with Twitter CLI t

## Setup

1. Fork repository
2. Create a new application for your twitter account in order to generate the variables needed at https://apps.twitter.com/app/new
3. Find the variables of your twitter app at `https://apps.twitter.com/app/<APPID>/keys`
4. Create the following secret variables for your project
  - TWITTER_USERNAME = `Owner`
  - TWITTER_CONSUMER_KEY = `Consumer Key (API Key)`
  - TWITTER_CONSUMER_SECRET = `Consumer Secret (API Secret)`
  - TWITTER_TOKEN = `Access Token`
  - TWITTER_SECRET = `Access Token Secret`

## Usage

1. Create a new Merge Request
2. Edit the contents of `tweet-text.md`
3. Replace `tweet-image.jpg` with a different image (named exactly the same) or remove it
4. Push and Merge
5. See tweet live
6. Invite other users to tweet via this project by allowing them to create Merge Requests as `developers`.

## Links

[Twitter CLI t](https://github.com/sferik/t)
